# README #

Infamous game called the Solo Test. It's like a single player checkers.

### Setup ###

In Debug there is a compiled version of the game for Windows(Somewhat trained version exists in release). 
Or you can just simply compile the main in ConsoleApplication4.

### How to play ###

E = Select Pin
WASD = Directions
Q = Cancel Selection

Remove a pin by jumping over another. Goal is to leave as few pins as possible.