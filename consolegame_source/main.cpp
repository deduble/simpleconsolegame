#include <stdio.h>
#include <cassert>
#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <cstdlib>
#include <Windows.h>
#include <conio.h>
#include "solotest.h"
#include <fstream>
#include "pin.h"
#include "Burrow.h"
#include "ConsoleDesign.h"
#include "learner.h"
#include <map>
#include <tuple>


//Meaning of values on vectors
// '0' is an empty burrow/cell
// '1' is a pin
// '3' is the selected pin
// '5' is an unplayable area
// Player marker is a 'color highlight' that moves over the board
// RULES : Move a pin over another to an empty cell. That way
//		   you remove the pin you jumped over. Goal is to leave as few pins as possible. 

//Has on development MDP implementation. See 'learner' files.(https://www.cs.rice.edu/~vardi/dag01/givan1.pdf)
//Currently plays off-policy, and it will until I make sure Q table optimizes at least somewhat good.
//Bot has no idea about the player marker.
using namespace std;

struct stepInf {
	bool GoalReached = FALSE;
	float reward = 0;
	string nextstate;
	int pinsLeft = 32;
} obs;

int main()
{
	START:
	console::setup();
	console::ConsoleResolution(15, 20);
	cout << "Option 1: Bot play \n Option 2: Human-play \n Enter :";
	console::showCursor(FALSE);
	int option;
	cin >> option;
	console::ConsoleResolution(60, 70);
	switch (option) {
	case 1: {			
		learner * bot = new learner;
		for (int episode = 0; episode != 50; ++episode) {
			solotest *pSoloTest = new solotest(7, 7);
			for (int step = 0; obs.GoalReached==FALSE; ++step) {
				pSoloTest->implementActionFromOutside(bot->randomAction());
				pSoloTest->setNextState();
				//pSoloTest->DrawCharBoard(); //To render Bot Actions
				obs.GoalReached = pSoloTest->pStepinf.GoalReached;
				obs.reward = pSoloTest->pStepinf.reward;
				obs.nextstate = pSoloTest->pStepinf.nextstate;
				obs.pinsLeft = pSoloTest->pStepinf.pinsLeft;
				pSoloTest->resetStructValues();
				pSoloTest->DrawCharBoard();
				bot->basicQlearner(obs.nextstate, obs.reward, obs.GoalReached);
			}
			bot->info();
			system("CLS");
			cout << "Episode "<<episode<< " finished.";
			obs.GoalReached = FALSE;
			delete pSoloTest;
		}
	}
	case 2:
		solotest *pSoloTest = new solotest(7, 7);
		pSoloTest->GameLogic();
		delete pSoloTest;
		goto START;
		break;
	}
}