#pragma once
#include <Windows.h>
#include <conio.h>
namespace console
{
	void cls();
	void ConsoleResolution(int FontWidth, int FontHeight);//Console should Run in Legacy Mode
	void showCursor(bool show);
	void setup();
	void gotoxy(short x, short y);
	void clear();
	void setColor(unsigned char color);
	void resetColor();
}