#pragma once
#include <stdio.h>
#include <cassert>
#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <cstdlib>
#include <Windows.h>
#include <conio.h>
#include <fstream>
#include "pin.h"
#include "Burrow.h"
#include "ConsoleDesign.h"
#include "learner.h"
#include <map>

class solotest
	: public pin, Burrow
{
private:
	void Input();
	enum Controls { PLAY, LEFT, RIGHT, UP, DOWN, CANCEL };
	Controls move;
public:
	struct stepInf {
		bool GoalReached=FALSE;
		float reward=-0.04;
		string nextstate;
		int pinsLeft=32;
	};
	stepInf pStepinf;
	bool pinRemoved = FALSE;
	bool Selected, GameStatus;
	int *Marker = NULL;
	int height, width;
	int cordX, cordY;
	int MarkedCellCoordX, MarkedCellCoordY;
	vector < vector<int > > GameBoard;
	solotest(int constructorHeight, int constructorWidth);
	void createBoard(int nullAreaDesign);
	void DrawDebugIntBoard();
	void DrawCharBoard();
	void GameLogic();
	void PlayerMarker();
	void PlaySelected(int markedX, int markedY);
	void Deselect();
	void resetStructValues();
	bool VerticalRemoveAvailable();
	bool HorizontalRemoveAvailable();
	bool AnyMovesLeft();
	bool BotAnyMovesLeft();
	bool GameOver(bool GameStatus);
	string setNextState();
	void implementActionFromOutside(int _action);
	~solotest();
};
