#include "learner.h"

learner::learner(): 
numberOfActions(6),
currentState("111111111111111101111111111111111"),
alpha(0.94),
gammaValue(0.8),
NumOfSteps(120), 
NumOfEpisodes(50),
rewardPerStep(0),
rewardPerEpisode(0)
{
	randomAction();
	initializeQTable(numberOfActions);
	initializeRTable(numberOfActions);
	currentQvalue = Qtable[Action][currentState];
	currentRvalue = Rtable[Action][currentState];
}
static unsigned long x = 123456789, y = 362436069, z = 521288629;
void learner::basicQlearner(string nextstate, float reward, bool done) {
	rewardPerStep = reward;
	nextState = nextstate;
	generateQtableZeros();
	auto& Ritr = Rtable[Action].find(currentState); {
		if (Ritr != Rtable[Action].end())
			Ritr->second = rewardPerStep;
		else
			Rtable[Action].insert(pair<string, float>(currentState, rewardPerStep));
	}
	Qtable[Action][currentState] = calculateQvalue();
	rewardPerStep = 0;
	currentState = nextState;
}
int learner::learnerBestAction() {
	float maxQ = 0;
	int bestAction = 0;
	for (int action = 0; action != numberOfActions; ++action) {
		if (maxQ < Qtable[action][currentState]) {
			maxQ = Qtable[action][currentState]; 
			bestAction = action; 
		}
	}
	return (maxQ == 0 ? randomAction() : bestAction);
}
float learner::NextMaxQvalue() {
	map<string, float>::iterator itr;
	float highest = 0;
	for (auto& allActions : Qtable) {
		itr = allActions.second.find(nextState);
		if (itr != allActions.second.end() && itr->second > highest) {
			highest = itr->second;
		}
	}
	return highest;
}
unsigned long Randm(void) {          //period 2^96-1
	unsigned long t;
	x ^= x << 16;
	x ^= x >> 5;
	x ^= x << 1;

	t = x;
	x = y;
	y = z;
	z = t ^ x ^ y;

	return z;
}
float learner::calculateQvalue() 
{
	currentQvalue = Qtable[Action][currentState];
	currentRvalue = Rtable[Action][currentState];
	float MaxQsaNext = NextMaxQvalue();
	float Gamma;
	Gamma = gammaValue;
	float newQ = 0;
	newQ = (1-alpha)*currentQvalue + alpha*(currentRvalue + Gamma*(MaxQsaNext));
	return newQ;
}
void learner::initializeQTable(int Actions) {
	innerZEROSRmap.insert(pair<string, float>(currentState, 0));
	for(int a = 0; a < numberOfActions; ++a){
		Qtable.insert(pair<int, map <string, float> >(a, innerZEROSRmap));
	}
}
void learner::initializeRTable(int Actions) {
	innerSRmap.insert(pair<string, float>(currentState, (-0.04)));
	for (int a = 0; a < numberOfActions; ++a) {
		Rtable.insert(pair<int, map <string, float> >(a, innerSRmap));
	}
}
void learner::generateQtableZeros() {
	for (int _AllActions = 0; _AllActions != numberOfActions; ++_AllActions) {
		auto& actionitr = Qtable.find(_AllActions);
		if (actionitr != Qtable.end()) {
			auto& stateitr = actionitr->second.find(nextState);
			if (stateitr == actionitr->second.end()) {
				//this code will insert '0' values on the
				//Q table for nextstate for ALL ACTIONS, 'UNLESS' there is already a value
				actionitr->second.insert(pair<string, float >(nextState, 0));
			}
		}
	}
}
int learner::randomAction(){
	Action = Randm() % numberOfActions;
	return Action;
}
void learner::info()
{
	LOG.open("infoLog.txt", ios::trunc);
	LOG << time(0) << endl;
	for (auto & itr1 : Qtable) {
		for (auto& itr2 : itr1.second) {
			LOG <<"Action : "<< itr1.first <<"| |" <<"State :" << itr2.first<< "||" <<"Q-value : "<<itr2.second<<endl;
		}
	}
	LOG.close();
}
void learner::checkPointSaver() {
	LOG.open("TextLOG.txt", ios::out);
	for (auto & itr1 : Qtable) {
		for (auto& itr2 : itr1.second) {
			LOG << itr1.first << endl << itr2.first << endl << itr2.second<<endl;
		}
	}
	LOG.close();
}
void learner::checkPointLoader(){
	string line;
	LOG.open("TextLOG.txt", ios::in);
	LOG.close();
}
learner::~learner()
{}
