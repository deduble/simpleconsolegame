#pragma once
#include "pin.h"
#include "burrow.h"
#include <vector>
#include <map>
#include <fstream>
#include <string>
#include <cstdlib>
#include <Windows.h>
#include <ctime>
#include <tuple>

class learner
{
private:
	int learnerBestAction();
	void initializeQTable(int Actions);
	void initializeRTable(int Actions);
	float calculateQvalue();
	void generateQtableZeros();
	float NextMaxQvalue();
	map <int, map <string, float > > Qtable;
	map <int, map <string, float > > Rtable;
	map<string, float>innerZEROSRmap;
	map<string, float>innerSRmap;
	bool GoalReached = FALSE;
	int AssumedQ = 0;
	fstream LOG;
	static unsigned long x, y, z;
	int NumOfEpisodes, NumOfSteps, numberOfActions;
	float rewardPerEpisode, rewardPerStep, currentRvalue, currentQvalue;
	string nextState, currentState;
	float gammaValue, alpha;
	void checkPointSaver();
	void checkPointLoader();
	
public:
	learner();
	void info();
	int Action, randomAction();
	void basicQlearner(string nextState, float reward, bool done);
	~learner();
};