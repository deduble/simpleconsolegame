﻿#pragma once
#include "pin.h"

using namespace std;
bool pin::pinSelected(int X, int Y)
{
	return ((X && Y) == 3);
}
char pin::DrawSelectedPin(bool Highlight)
{
	console::setColor(Highlight ? '0x25' : '0x29');
	cout << char(248);
	return char(248);
}
char pin::drawPin(bool Highlight = 0)
{
	console::setColor(Highlight ? '0x02' : '0x76');
	cout << char(248);
	console::resetColor();
	return char(248);
}
pin::pin() : PinsLeft(32)
{
}
pin::~pin()
{
}
