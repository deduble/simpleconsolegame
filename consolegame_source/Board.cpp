﻿#include <stdio.h>
#include <string>
#include <cmath>
#include <algorithm>
#include <vector>
#include <iostream>
#include <Windows.h>
#include <conio.h>

using namespace std;

void fontSize(int x = 24, int y = 30)
{
	CONSOLE_FONT_INFOEX cfi;
	cfi.cbSize = sizeof(cfi);
	cfi.nFont = 0;
	cfi.dwFontSize.X = x;                  // Width of each character in the font
	cfi.dwFontSize.Y = y;                  // Height
	cfi.FontFamily = FF_DONTCARE;
	cfi.FontWeight = FW_NORMAL;
	std::wcscpy(cfi.FaceName, L"Consolas"); // Choose your font
	SetCurrentConsoleFontEx(GetStdHandle(STD_OUTPUT_HANDLE), FALSE, &cfi);
}

class SoloTest
{
private:
	enum Controls { SELECT, LEFT, RIGHT, UP, DOWN };
public:
	int height, width;
		int cordX, cordY;
			int cells, pins;
	bool GameStatus;
	int* Selector = &GameBoard[cordX][cordY];
	vector < vector < int > > GameBoard;
		vector < vector < int > > SelectorBoard;
	Controls move;
	SoloTest(int constructorHeight, int constructorWidth) : 
		height(7), width(7), cordX(3), cordY(3), cells(33), pins(32), GameStatus(TRUE)
	{
		SoloTest::height = constructorHeight;
		SoloTest::width = constructorWidth;
		createBoard(1);
		Draw();
	}
	void createBoard(int nullAreaDesign)
	{
		vector< vector< int > > tempVec (height, vector < int >(width));
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				tempVec[i][j] = 1;
			}
		}
		switch (nullAreaDesign)
		{
		default:				//Puts four of 2x2 null areas to each corner
			tempVec[0][0] = tempVec[0][1] = tempVec[0][5] = tempVec[0][6] = 0;
			tempVec[1][0] = tempVec[1][1] = tempVec[1][5] = tempVec[1][6] = 0;
			tempVec[5][0] = tempVec[5][1] = tempVec[5][5] = tempVec[5][6] = 0;
			tempVec[6][0] = tempVec[6][1] = tempVec[6][5] = tempVec[6][6] = 0;
			tempVec[3][3] = 5;
			break;
		}
		GameBoard = tempVec;
	};
	void Draw()
	{
		system("CLS");
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
					cout << GameBoard[i][j];
			}
			cout << endl;
		}
	};
	void Input()
	{
		if (_kbhit)
		{
			switch (_getch())
			{
			case 'w':
				move = UP;
				break;
			case 'a':
				move = LEFT;
				break;
			case 'd':
				move = RIGHT;
				break;
			case 's':
				move = DOWN;
				break;
			case ' ':
				move = SELECT;
			}
		}
	}
	bool GameLogic() 
	{

	}
};
int main()
{	
	fontSize(30, 30);
	SoloTest game(7, 7);
	return 0;
}