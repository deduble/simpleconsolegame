#include "ConsoleDesign.h"
#include <iostream>
using namespace std;

HANDLE consoleHandle;
void console::cls()
{
	// Get the Win32 handle representing standard output.
	// This generally only has to be done once, so we make it static.
	static const HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_SCREEN_BUFFER_INFO csbi;
	COORD topLeft = { 0, 0 };

	// std::cout uses a buffer to batch writes to the underlying console.
	// We need to flush that to the console because we're circumventing
	// std::cout entirely; after we clear the console, we don't want
	// stale buffered text to randomly be written out.
	std::cout.flush();

	// Figure out the current width and height of the console window
	if (!GetConsoleScreenBufferInfo(hOut, &csbi)) {
		// TODO: Handle failure!
		abort();
	}
	DWORD length = csbi.dwSize.X * csbi.dwSize.Y;

	DWORD written;

	// Flood-fill the console with spaces to clear it
	FillConsoleOutputCharacter(hOut, TEXT(' '), length, topLeft, &written);

	// Reset the attributes of every character to the default.
	// This clears all background colour formatting, if any.
	FillConsoleOutputAttribute(hOut, csbi.wAttributes, length, topLeft, &written);

	// Move the cursor back to the top left for the next sequence of writes
	SetConsoleCursorPosition(hOut, topLeft);
}

void console::ConsoleResolution(int FontWidth = 200, int FontHeight = 200)
{
	CONSOLE_FONT_INFOEX cfi;
	cfi.cbSize = sizeof(cfi);
	cfi.nFont = 0;
	cfi.dwFontSize.X = FontWidth;                  // Width of each character in the font
	cfi.dwFontSize.Y = FontHeight;                  // Height
	cfi.FontFamily = FF_DONTCARE;
	cfi.FontWeight = FW_NORMAL;
	wcscpy(cfi.FaceName, L"Consolas"); // Choose your font
	SetCurrentConsoleFontEx(GetStdHandle(STD_OUTPUT_HANDLE), FALSE, &cfi);
}
void console::setup()
{
	consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);

	showCursor(false);
}

void console::showCursor(bool show)
{
	const CONSOLE_CURSOR_INFO cinfo = { 25 , show };

	SetConsoleCursorInfo(consoleHandle, &cinfo);
}

void console::clear()
{
	system("cls");
}

void console::gotoxy(short x, short y)
{
	COORD pos = { x, y };
	SetConsoleCursorPosition(consoleHandle, pos);
}

void console::setColor(unsigned char color)
{
	SetConsoleTextAttribute(consoleHandle, color);
}

void console::resetColor()
{
	SetConsoleTextAttribute(consoleHandle, 0x08);
}