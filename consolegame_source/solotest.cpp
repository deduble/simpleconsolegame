#include "solotest.h"

void solotest::Input()
{
	switch (_getch())
	{
	case 'w':
		move = UP;
		break;
	case 'a':
		move = LEFT;
		break;
	case 'd':
		move = RIGHT;
		break;
	case 's':
		move = DOWN;
		break;
	case 'e':
		move = PLAY;
		break;
	case 'q':
		move = CANCEL;
		break;

	}

}
solotest::solotest(int constructorHeight, int constructorWidth) :
	GameStatus(TRUE), 
	Selected(FALSE), 
	height(7), width(7), 
	cordX(3), cordY(3)
{
	height = constructorHeight;
	width = constructorWidth;
	createBoard(1);
	DrawCharBoard();
	pStepinf.nextstate = setNextState();
}
void solotest::GameLogic()
{
	while (GameStatus) {
		PlayerMarker();
		//AutomatedPlay();
		DrawCharBoard();
	}
	GameOver(GameStatus);
}
void solotest::createBoard(int nullAreaDesign) {
	vector< vector< int > > tempVec(height, vector < int >(width));
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			tempVec[i][j] = 1;
		}
	}
	switch (nullAreaDesign)
	{
	case 0:
	{
		srand(GetTickCount());
		for (int i = 0; i < 15; ++i)
		{
			tempVec[rand() % height][rand() % width] = 5;
		}

		break;
	}
	default: {			//Puts four square unplayable areas to each corner
		tempVec[0][0] = tempVec[0][1] = tempVec[0][5] = tempVec[0][6] = 5;
		tempVec[1][0] = tempVec[1][1] = tempVec[1][5] = tempVec[1][6] = 5;
		tempVec[5][0] = tempVec[5][1] = tempVec[5][5] = tempVec[5][6] = 5;
		tempVec[6][0] = tempVec[6][1] = tempVec[6][5] = tempVec[6][6] = 5;
		tempVec[3][3] = 0;
		break;
	}
	}
	GameBoard = tempVec;
}
void solotest::DrawDebugIntBoard()
{
	system("CLS");
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			cout << GameBoard[i][j];
		}
		cout << endl;
	}
};
void solotest::DrawCharBoard()
{
	system("cls");
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			bool Highlight = { (i == cordX) && (j == cordY) };
			switch (GameBoard[i][j])
			{
			case 0:
				drawBurrow(Highlight);
				break;
			case 1:
				drawPin(Highlight);
				break;
			case 3:
				DrawSelectedPin(Highlight);
				break;
			case 5:
				console::setColor('0x68');
				cout << (char)219;
				break;
			}
		}
		cout << endl;
	}
	console::resetColor();
}
void solotest::PlayerMarker() {
	Input();//Gets a keypress
	switch (move) {
	case PLAY://Play key has 3 functionality:
			  //1 )It selects a pin and assigns it to *Marker, 2) moves a selected pin or 3) deselects when chosen the selected pin)
		switch (Selected) {

		case FALSE: {
			if (GameBoard[cordX][cordY] != 0) {
				Selected = TRUE;
				PlaySelected(cordX, cordY);
				break;
			}
			else
				break;
		}
		case TRUE: {
			if (Marker == &GameBoard[cordX][cordY]) {
				Deselect();
				break;
			}
			else if (VerticalRemoveAvailable()) {
				GameBoard[MarkedCellCoordX][MarkedCellCoordY] = 0;
				GameBoard[cordX][cordY] = 1;
				GameBoard[((MarkedCellCoordX + cordX) / 2)][cordY] = 0;
				Selected = FALSE;
				PinsLeft--;
				Marker = NULL;
				GameStatus = AnyMovesLeft();
				break;
			}//Vertical
			else if (HorizontalRemoveAvailable()) {
				GameBoard[MarkedCellCoordX][MarkedCellCoordY] = 0;
				GameBoard[cordX][cordY] = 1;
				GameBoard[cordX][((MarkedCellCoordY + cordY) / 2)] = 0;
				Marker = NULL;
				Selected = FALSE;
				PinsLeft--;
				GameStatus = AnyMovesLeft();
				break;
			}//Horizontal
			else
				break;
		}
				   break;
		}
		break;
	case UP:
		if (cordX == 0) {
			cordX += (height - 1);
			break;
		}
		else if (GameBoard[cordX - 1][cordY] != 5) {
			cordX--;
		}
		break;
	case DOWN:
		if (cordX == (height - 1)) {
			cordX = 0;
			break;
		}
		else if (GameBoard[cordX + 1][cordY] != 5) {
			cordX++;
		}
		break;
	case LEFT:
		if (cordY == 0) {
			cordY += (width - 1);
			break;
		}
		else if (GameBoard[cordX][cordY - 1] != 5) {
			cordY--;
		}
		break;
	case RIGHT:
		if (cordY == (width - 1)) {
			cordY = 0;
			break;
		}
		else if (GameBoard[cordX][cordY + 1] != 5) {
			cordY++;
			break;
		}
		break;
	case CANCEL: {
		if (Selected) {
			*Marker = 1;
			Marker = NULL;
			Selected = FALSE;
			break;
		}
		else
			break;
	}
	default:
		break;
	}
}
void solotest::PlaySelected(int markedX, int markedY)
{
	MarkedCellCoordX = markedX;
	MarkedCellCoordY = markedY;
	Marker = &GameBoard[cordX][cordY];
	*Marker = 3;
}
void solotest::Deselect()
{
	*Marker = 1;
	Marker = NULL;
	Selected = FALSE;
}
bool solotest::VerticalRemoveAvailable() {
	return (GameBoard[cordX][cordY] == 0 &&
		(*Marker + GameBoard[(MarkedCellCoordX + cordX) / 2][cordY] + GameBoard[cordX][cordY] == 4) &&
		(MarkedCellCoordY == cordY) && (cordX - MarkedCellCoordX == 2 || cordX - MarkedCellCoordX == -2));
}
bool solotest::HorizontalRemoveAvailable() {
	return (GameBoard[cordX][cordY] == 0 &&
		(*Marker + GameBoard[cordX][((MarkedCellCoordY + cordY) / 2)] + GameBoard[cordX][cordY] == 4) &&
		(MarkedCellCoordX == cordX) && (cordY - MarkedCellCoordY == 2 || cordY - MarkedCellCoordY == -2));
}
bool solotest::AnyMovesLeft()
{
	for (int x = 0; x != GameBoard.size() - 2; ++x) {
		for (int y = 0; y != GameBoard.front().size() - 2; ++y) {
			if (GameBoard[x][y] + GameBoard[x][y + 1] + GameBoard[x][y + 2] == 2 && GameBoard[x][y + 1] != 0) {
				return TRUE;
			}
		}
		for (int y = 0; y != GameBoard.front().size(); ++y) {
			if (GameBoard[x][y] + GameBoard[x + 1][y] + GameBoard[x + 2][y] == 2 && GameBoard[x + 1][y] != 0) {
				return TRUE;
			}
		}
	}
	return FALSE;
}
bool solotest::BotAnyMovesLeft()
{
	for (int x = 0; x != GameBoard.size() - 2; ++x) {
		for (int y = 0; y != GameBoard.front().size() - 2; ++y) {
			if (GameBoard[x][y] + GameBoard[x][y + 1] + GameBoard[x][y + 2] == 2 && GameBoard[x][y + 1] != 0) {
				return TRUE;
			}
		}
		for (int y = 0; y != GameBoard.front().size(); ++y) {
			if (GameBoard[x][y] + GameBoard[x + 1][y] + GameBoard[x + 2][y] == 2 && GameBoard[x + 1][y] != 0) {
				return TRUE;
			}
		}
	}
	return FALSE;
}
string solotest::setNextState() {
	string HashVal;
	unsigned int Cell = 0;
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			if (GameBoard[i][j] != 5) {
				Cell = GameBoard[i][j];
				HashVal += to_string(Cell);
			}
		}
	}
	HashVal += to_string(cordX) + to_string(cordY);
	pStepinf.nextstate = HashVal;
	return HashVal;
}
bool solotest::GameOver(bool _GameStatus)
{
	console::ConsoleResolution(30, 30);
	console::setColor('0x49');
	cout << "Game Over" << endl << "Pins Left: " << PinsLeft << endl;
	cout << "Enter '1' to start a new game \n Enter something else to quit" << endl;
	try
	{
		cin >> _GameStatus;
		if (_GameStatus != 1)
			throw 15;
		else
			GameBoard.clear();
			solotest::solotest(7, 7);
			PinsLeft = 32;
	}
	catch (int)
	{
		cout << "...";
		exit(1);
		return 0;
	}
}
void solotest::implementActionFromOutside(int _action) {
	switch (_action)
	{
	case 0://Play key has 3 functionality:
		   //1) It selects a pin and assigns it to *Marker, 2) moves a selected pin or 3) deselects when chosen the selected pin)
		switch (Selected)
		{
		case FALSE:
		{
			if (GameBoard[cordX][cordY] != 0)
			{
				Selected = TRUE;
				PlaySelected(cordX, cordY);
				break;
			}
			else
				break;
		}
		case TRUE:
		{
			if (Marker == &GameBoard[cordX][cordY])
			{
				Deselect();
				break;
			}
			else if (VerticalRemoveAvailable())
			{
				GameBoard[MarkedCellCoordX][MarkedCellCoordY] = 0;
				GameBoard[cordX][cordY] = 1;
				GameBoard[((MarkedCellCoordX + cordX) / 2)][cordY] = 0;
				Selected = FALSE;
				PinsLeft--;
				Marker = NULL;
				pStepinf.GoalReached = !BotAnyMovesLeft();
				pStepinf.reward += 1;
				break;
			}//Vertical
			else if (HorizontalRemoveAvailable())
			{
				GameBoard[MarkedCellCoordX][MarkedCellCoordY] = 0;
				GameBoard[cordX][cordY] = 1;
				GameBoard[cordX][((MarkedCellCoordY + cordY) / 2)] = 0;
				Marker = NULL;
				Selected = FALSE;
				PinsLeft--;
				pStepinf.GoalReached = !BotAnyMovesLeft();
				pStepinf.reward += 1;
				break;
			}//Horizontal
			else
				break;
		}
		break;
		}
		break;
	case 1:
		if (cordX == 0)
		{
			cordX += (height - 1);
			break;
		}
		else if (GameBoard[cordX - 1][cordY] != 5)
		{
			cordX--;
		}
		break;
	case 2:
		if (cordX == (height - 1))
		{
			cordX = 0;
			break;
		}
		else if (GameBoard[cordX + 1][cordY] != 5)
		{
			cordX++;
		}
		break;
	case 3:
		if (cordY == 0)
		{
			cordY += (width - 1);
			break;
		}
		else if (GameBoard[cordX][cordY - 1] != 5)
		{
			cordY--;
		}
		break;
	case 4:
		if (cordY == (width - 1))
		{
			cordY = 0;
			break;
		}
		else if (GameBoard[cordX][cordY + 1] != 5)
		{
			cordY++;
			break;
		}
		break;
	case 5:
	{
		if (Selected)
		{
			*Marker = 1;
			Marker = NULL;
			Selected = FALSE;
			break;
		}
		else break;
	}
	default:
		break;
	}
	//DrawCharBoard();
}
void solotest::resetStructValues() {
	pStepinf.GoalReached = FALSE;
	pStepinf.reward = -0.04;
	pStepinf.nextstate;
	pStepinf.pinsLeft = 32;
};
solotest::~solotest()
{
	GameBoard.clear();
	system("CLS");
}
